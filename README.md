# CubeSat-CentralisedClockSource

For all clock users, only the files in the listener dir matter.

The centralised clock is implemented on a customized monodirectional two wire bus modified from the popular I2C protocol, users will need 2 pin to be able to listen the broadcast from the clock source.
On current setting, the source is sending clock data at rate of 15.5kHz. Unfortunately the data integrity is not so great on this bus so always look at checksum result before accepting data from this bus.
Sample projects are now provided.
Current data form(all in binary): HH, MM, SS, DD, mm, YY