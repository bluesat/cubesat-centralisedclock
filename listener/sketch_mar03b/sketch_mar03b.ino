#include <Timestamp.h>

#include "Timestamp.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  init_getTimeStamp(2,3);
}

void loop() {
  // put your main code here, to run repeatedly:
  byte timeStamp[6];
  byte timeStr[12];
  int success = getTimeStamp(timeStamp, 6);
  binToTimeStr(timeStamp, timeStr);
  Serial.write(timeStr, 12);
  Serial.write(success ? " data curropted" : " data valid");
  Serial.write("\n");
  delay(10);
}

void binToTimeStr(byte* source, byte* buffer) {
  for(int i = 0; i < 6; i++) {
    buffer[2 * i] = source[i] / 10 + '0';
    buffer[2 * i + 1] = source[i] % 10 + '0';
  }
}
