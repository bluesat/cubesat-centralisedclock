#include <Timestamp.h>

#include <Wire.h>
#include <DS3231.h>

DS3231* rtc;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  init_broadcase(2, 3);
  randomSeed(analogRead(0));
  rtc = new DS3231(SDA, SCL);
  char time[6];
  Serial.write("update DS3231 time by the following format (binary): HH, MM, SS, DD, MM, YY");
  Serial.readBytes(time, 6);
  rtc->setTime(time[0],time[1],time[2]);
  rtc->setDate(time[3],time[4],time[5] + 2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Time timeStu= rtc->getTime();
  byte timeStamp[6] = {timeStu.hour, timeStu.min, timeStu.sec, timeStu.date, timeStu.mon, timeStu.year - 2000};
  broadcast(timeStamp, 6, 2, 3);
  Serial.write(rtc->getTimeStr());
  Serial.write(rtc->getDateStr());
  Serial.write("\n");
}

//TEMP implementation
void init_broadcase(int SDA, int SCL) {
  //init
  pinMode(SDA, OUTPUT);
  pinMode(SCL, OUTPUT);
  digitalWrite(SDA, HIGH);
  digitalWrite(SCL, HIGH);
  delay(100);
}

//dataform: n-bytes are transmitted in I2C style (without master and ack), from MSB to LSB and no intervals between bytes
void broadcast(byte* data, int len, int SDA, int SCL) {

  //start transmission
  digitalWrite(SDA, LOW); //pull SDA from high to low while SCL is high
  delayMicroseconds(8); //expected transmission rate: ?hz
  //Serial.write("Start transmission\n");
  //
  byte temp = 0x00;
  byte sum = 0x00;
  const byte mask = 0x80;
  for (int byteCount = 0; byteCount <= len; byteCount++) {
    if(byteCount < len) {
      temp = data[byteCount];
      sum += temp;
    } else
      temp =  sum;
    //Serial.write(" transmitting: ");
    //Serial.write(temp);
    for (int bitCount = 7; bitCount >= 0; bitCount--) { //doing my magic
      digitalWrite(SCL, LOW);
      delayMicroseconds(8);
      digitalWrite(SDA, mask & temp ? HIGH : LOW);
      delayMicroseconds(8);
      digitalWrite(SCL, HIGH);
      delayMicroseconds(8);
      temp <<= 1;
    }
    delayMicroseconds(8);
  }
  //end transmission
  //Serial.write("end transmission");
  delayMicroseconds(8);
  digitalWrite(SCL, LOW);
  delayMicroseconds(8);
  digitalWrite(SDA, LOW);
  delayMicroseconds(8);
  digitalWrite(SCL, HIGH);
  delayMicroseconds(8);
  digitalWrite(SDA, HIGH);
}
